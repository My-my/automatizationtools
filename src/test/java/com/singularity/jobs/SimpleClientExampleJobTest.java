package com.singularity.jobs;

import org.junit.Test;

/**
 * Created by my-my
 * Date: 05.09.15
 * Time: 14:49
 */
public class SimpleClientExampleJobTest {

    @Test
    public void testProcess() throws Exception {

        SimpleClientExampleJob job = new SimpleClientExampleJob(
                "Simple client",
                "http://developer.alexanderklimov.ru/android/java/generic.php",
                10 * 1000
        );

        job.process();
    }
}