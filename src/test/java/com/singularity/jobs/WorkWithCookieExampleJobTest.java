package com.singularity.jobs;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Дмитрий
 * Date: 07.09.15
 * Time: 0:37
 * To change this template use File | Settings | File Templates.
 */
public class WorkWithCookieExampleJobTest {
    @Test
    public void testProcess() throws Exception {
        WorkWithCookieExampleJob job = new WorkWithCookieExampleJob("Coookies job example", "http://localhost", 60 * 1000);
        job.process();
    }
}
