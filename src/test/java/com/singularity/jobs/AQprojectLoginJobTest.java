package com.singularity.jobs;

import org.junit.Test;

/**
 * Created by my-my
 * Date: 05.09.15
 * Time: 14:53
 */
public class AQprojectLoginJobTest {

    @Test
    public void testProcess() throws Exception {

        AQprojectLoginJob job = new AQprojectLoginJob(
                "AQLogin",
                "http://torrent.aqproject.ru/",
                10 * 1000
        );

        job.process();
    }
}