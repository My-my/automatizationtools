package com.singularity.tools.http.client;

import org.apache.http.Header;

import java.util.*;

/**
 * Created by my-my
 * Date: 05.09.15
 * Time: 2:21
 */
public class ClientResponse {
    private int status;
    private List<Header> headers;
    private Object content;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public Header[] getHeadersAsArray() {
        return headers.toArray(new Header[headers.size()]);
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public void setHeaders(Header[] headers) {
        this.headers = Arrays.asList(headers);
    }
}
