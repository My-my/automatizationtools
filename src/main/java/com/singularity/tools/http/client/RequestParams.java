package com.singularity.tools.http.client;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by my-my
 * Date: 05.09.15
 * Time: 1:30
 */
public class RequestParams {
    private List<NameValuePair> params = new ArrayList<>();

    public List<NameValuePair> getParams() {
        return params;
    }

    public void addParam(String key, String value){
        params.add(new BasicNameValuePair(key, value));
    }
}
