package com.singularity.tools.http.client;

import com.singularity.tools.http.RestCookie;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by my-my
 * Date: 02.09.15
 * Time: 0:28
 */
public class RestClient {
    private HttpClient client;
    private List<Header> headers = new ArrayList<Header>();
    private BasicCookieStore cookieStore = null;

    public RestClient(){
        cookieStore = new BasicCookieStore();
        client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
        headers.add(new BasicHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"));
    }

    public ClientResponse getQuery(String url) {
        return getQuery(url, new RequestParams());
    }

    public ClientResponse getQuery(String url, RequestParams params) {
        HttpGet request = new HttpGet(url);
        setupHeaders(request);
        setGetParams(request, params);

        ClientResponse response = executeRequest(request);

        return response;
    }

    public ClientResponse postQuery(String url) {
        return postQuery(url, new RequestParams());
    }

    public ClientResponse postQuery(String url, RequestParams params){
        HttpPost request = new HttpPost(url);
        setupHeaders(request);
        setPostParams(request, params);

        ClientResponse response = executeRequest(request);

        return response;
    }

    public ClientResponse postQuery(String url, String json){
        HttpPost request = new HttpPost(url);
        setupHeaders(request);
        setPostJSON(request, json);

        ClientResponse response = executeRequest(request);

        return response;
    }

    private ClientResponse executeRequest(HttpRequestBase request){
        ClientResponse result = new ClientResponse();

        try {
            HttpResponse response = client.execute(request);
            String content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
            result.setHeaders(response.getAllHeaders());
            result.setStatus(response.getStatusLine().getStatusCode());
            result.setContent(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<Header> getRequestHeaders(){
        return headers;
    }

    public List<Cookie> getCookies(){
        return cookieStore.getCookies();
    }

    public void addCookie(RestCookie cookie){
        cookieStore.addCookie(cookie);
    }

    public void clearCookies(){
        cookieStore.clear();
    }

    private void setupHeaders(HttpRequestBase request){
        for (Header header : headers){
            request.addHeader(header);
        }
    }

    private void setGetParams(HttpGet get, RequestParams params){
        try {
            URI uri = new URIBuilder(get.getURI()).addParameters(params.getParams()).build();
            get.setURI(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void setPostParams(HttpPost post, RequestParams params){
        try {
            post.setEntity(new UrlEncodedFormEntity(params.getParams(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void setPostJSON(HttpPost post, String json){
        StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
        post.setEntity(entity);

    }

    public void destroy(){
        this.client = null;
        this.cookieStore = null;
        this.headers = new ArrayList<Header>();
    }
}
