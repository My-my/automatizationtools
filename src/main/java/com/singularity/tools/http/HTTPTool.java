package com.singularity.tools.http;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by my-my
 * Date: 01.09.15
 * Time: 0:04
 */
public class HTTPTool {

    public static List<String> selectElementsByCSS(String content, String cssSelector){
        List<String> result = new ArrayList<>();

        Document document = Jsoup.parse(content);
        Elements elements = document.select(cssSelector);
        for (Element element : elements){
            result.add(element.html());
        }

        return result;
    }

    public static List<String> selectElementsByRegex(String content, String regex){
        List<String> result = new ArrayList<>();

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);

        while (matcher.find()) {
            result.add(content.substring(matcher.start(), matcher.end()));
        }

        return result;
    }
}
