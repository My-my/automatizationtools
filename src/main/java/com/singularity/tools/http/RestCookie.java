package com.singularity.tools.http;

import org.apache.http.impl.cookie.BasicClientCookie;

/**
 * Created with IntelliJ IDEA.
 * User: Дмитрий
 * Date: 08.09.15
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */
public class RestCookie extends BasicClientCookie{
    /**
     * Default Constructor taking a name and a value. The value may be null.
     *
     * @param name  The name.
     * @param value The value.
     */
    public RestCookie(String name, String value, String domain) {
        super(name, value);
        this.setDomain(domain);
    }
}
