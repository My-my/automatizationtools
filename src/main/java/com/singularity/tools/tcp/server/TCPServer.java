package com.singularity.tools.tcp.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lela
 * Date: 02.09.2015
 * Time: 13:09
 */
public class TCPServer {
    private ServerSocket serverSocket;
    private List<Socket> clients = new ArrayList<>();

    public void setup(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void shutdown() throws IOException {
        serverSocket.close();
    }

    public boolean acceptConnection() {
        try {
            clients.add(serverSocket.accept());
        } catch (IOException e) {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    public List<Socket> getClients(){
        return clients;
    }

    public boolean removeClient(Socket client) throws IOException {
        int clientId = clients.indexOf(client);

        if (clientId == -1)
            return false;

        client.close();
        clients.remove(client);

        return true;
    }

    public void sendToAll(String message) throws IOException {
        for (Socket client : clients) {
            DataOutputStream writer = new DataOutputStream(client.getOutputStream());
            writer.writeUTF(message);
            writer.flush();
        }
    }

    public List<String> getFromAll() throws IOException {
        List<String> result = new ArrayList<>();

        for (Socket client : clients) {
            String response = "";

            DataInputStream inputStream = new DataInputStream(client.getInputStream());

            while (inputStream.available() > 0) {
                String input = inputStream.readUTF();
                response += input;
            }

            result.add(response);
        }

        return result;
    }


}
