package com.singularity.tools.tcp.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Lela
 * Date: 02.09.2015
 * Time: 13:09
 */
public class TCPClient {
    private Socket socket;

    public void connect(String host, int port) throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
        socket = null;
    }

    public void send(String message) throws IOException {
        DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
        writer.writeUTF(message);
        writer.flush();
    }

    public DataInputStream getInputStream() throws IOException {
        DataInputStream inputStream = new DataInputStream(socket.getInputStream());

        return inputStream;
    }

    public String getAll() throws IOException {
        DataInputStream inputStream = getInputStream();
        String result = "";

        while (result.equals("")) {
            while (inputStream.available() > 0) {
                String input = inputStream.readUTF();
                result += input;
            }
        }

        return result;
    }


}
