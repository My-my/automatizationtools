package com.singularity.tools;

import com.google.gson.Gson;
import com.singularity.tools.http.client.ClientResponse;
import com.singularity.tools.http.client.RequestParams;
import com.singularity.tools.http.client.RestClient;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Дмитрий
 * Date: 10.09.15
 * Time: 2:45
 * To change this template use File | Settings | File Templates.
 */
public class FlagSubmitter {

    private static final String CHECKER_ADDRESS = "http://localhost/";

    public static String submit(String flag){
        RestClient client = new RestClient();

        String json = (new Gson()).toJson(Collections.singletonList(flag));

        ClientResponse response = client.postQuery(CHECKER_ADDRESS, json);

        return response.getContent().toString();
    }
}
