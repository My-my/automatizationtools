package com.singularity.tools.udp.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

/**
 * Created by Lela
 * Date: 05.09.2015
 * Time: 13:09
 */
public class UDPClient {
    private DatagramSocket socket;
    private InetAddress address;
    private int port;
    private int maxBufferSize = 10240;

    public void setupAddress(InetAddress address) {
        this.address = address;
    }

    public void setupPort(int port) {
        this.port = port;
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void send(String message) {
        byte[] sendData = message.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, port);
        try {
            socket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String recv() {
        try {

            byte[] receiveData = new byte[maxBufferSize];
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            socket.receive(receivePacket);

            return new String(receivePacket.getData(), 0, receivePacket.getLength(), StandardCharsets.UTF_8);
        } catch (IOException e){
            e.printStackTrace();
        }

        return "";
    }

    public void disconnect() {
        socket.close();
        socket = null;
    }

    public int getMaxBufferSize() {
        return maxBufferSize;
    }

    public void setMaxBufferSize(int maxBufferSize) {
        this.maxBufferSize = maxBufferSize;
    }
}
