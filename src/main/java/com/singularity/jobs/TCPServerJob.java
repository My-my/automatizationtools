package com.singularity.jobs;

import com.singularity.tools.tcp.server.TCPServer;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by my-my
 * Date: 01.09.15
 * Time: 0:07
 */
public class TCPServerJob extends Job {
    public TCPServerJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process() {
        TCPServer server = new TCPServer();
        try {
            server.setup(8080);

            print("Try to connect to client");
            boolean connected = server.acceptConnection();
            print("Connected = " + connected);

            Socket client = server.getClients().get(0);

            String message = server.getFromAll().get(0);
            print("Get message from client: " + message);

            String myMessage = "Hello, baby ;)";
            server.sendToAll(myMessage);
            print("Message sended");

            Thread.sleep(1000);

            myMessage = "Пошли есть! :)";
            server.sendToAll(myMessage);
            print("Message sended");

            print("remove clients, close server");
            server.removeClient(server.getClients().get(0));
            server.shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
