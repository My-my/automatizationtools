package com.singularity.jobs;

import com.singularity.tools.http.HTTPTool;
import com.singularity.tools.http.client.ClientResponse;
import com.singularity.tools.http.client.RestClient;

import java.util.List;

/**
 * Created by my-my
 * Date: 01.09.15
 * Time: 0:07
 */
public class SimpleClientExampleJob extends Job{
    public SimpleClientExampleJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process() {
        RestClient client = new RestClient();

        ClientResponse response = client.getQuery(address);
        String content = (String) response.getContent();

//        String reclama = HTTPTool.selectElementsByCSS(content, "h5").get(0);
//        print("From CSS select: " + reclama);



        List<String> reclama2 = HTTPTool.selectElementsByRegex(content, "Р(.)*а");
        print("From regex: "+reclama2);

        submitFlag(reclama2.get(0));

        client.destroy();
    }
}
