package com.singularity.jobs;

import com.singularity.tools.tcp.client.TCPClient;

import java.io.IOException;

/**
 * Created by my-my
 * Date: 01.09.15
 * Time: 0:07
 */
public class TCPClientJob extends Job{
    public TCPClientJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process(){
        TCPClient client = new TCPClient();
        try {
            client.connect("192.168.1.33", 8080);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String message = "Hello server, i'm client!";

        try {
            client.send(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

        print("message sended");

        try {
            String msg = "";
            while (msg.equals(""))
                msg = client.getAll();
            print(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }



        try {
            client.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
