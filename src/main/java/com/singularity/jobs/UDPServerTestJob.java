package com.singularity.jobs;

import com.singularity.tools.udp.client.UDPClient;

/**
 * Created by Lela on 05.09.2015.
 */
public class UDPServerTestJob extends Job {
    public UDPServerTestJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process() {
        UDPClient client = new UDPClient();
        client.setupPort(8080);

        String message = client.recv();

        print(message);
    }
}
