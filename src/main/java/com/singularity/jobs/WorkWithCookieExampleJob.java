package com.singularity.jobs;

import com.singularity.tools.http.HTTPTool;
import com.singularity.tools.http.RestCookie;
import com.singularity.tools.http.client.ClientResponse;
import com.singularity.tools.http.client.RestClient;
import org.apache.http.Header;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.BasicClientCookie2;

import java.util.List;

/**
 * Created by my-my
 * Date: 01.09.15
 * Time: 0:07
 */
public class WorkWithCookieExampleJob extends Job{
    public WorkWithCookieExampleJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process() {
        RestClient client = new RestClient();

        ClientResponse response = client.getQuery(address);
        RestCookie cookie = new RestCookie("My_cookie", "val of my cookie", "localhost");
        client.addCookie(cookie);

        response = client.getQuery(address);


        String content = (String) response.getContent();

        Integer status = response.getStatus();
        List<Header> headers = response.getHeaders();
        List<Cookie> cookies = client.getCookies();

        print(status.toString());
        print(headers.toString());
        print(cookies.toString());
        print("\n" + content);

        client.destroy();
    }
}
