package com.singularity.jobs;

import com.singularity.tools.http.client.ClientResponse;
import com.singularity.tools.http.client.RequestParams;
import com.singularity.tools.http.client.RestClient;

/**
 * Created by my-my
 * Date: 05.09.15
 * Time: 2:15
 */
public class AQprojectLoginJob extends Job {
    public AQprojectLoginJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process() {
        RestClient client = new RestClient();

        RequestParams params = new RequestParams();
        params.addParam("authlogin", "mymyps");
        params.addParam("authpassword", "incorrectPassword");

        ClientResponse response = client.postQuery(address + "/account/doLogin", params);
        print("Status = " + response.getStatus());

        response = client.getQuery(address + "/torrent/upload");
        print("Status = " + response.getStatus());

        print("req headers: " + client.getRequestHeaders());
        print("headers: " + response.getHeaders());
        print("cookies: " + client.getCookies());
        ;
    }
}
