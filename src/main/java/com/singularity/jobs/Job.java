package com.singularity.jobs;

import com.singularity.tools.FlagSubmitter;

import java.util.logging.Logger;

/**
 * Created by my-my
 * Date: 02.09.15
 * Time: 0:55
 */
public abstract class Job implements Runnable{
    protected String address;
    private long frequency;
    private String threadName;

//    private Logger LOGGER = Logger.getLogger("Job");

    public Job(String threadName, String address, long frequency){
        this.address = address;
        this.frequency = frequency;
        this.threadName = threadName;

//        LOGGER = Logger.getLogger(threadName+": ");
    }

    protected void print(String message){
//        LOGGER.info(message);
        System.out.println(String.format("%s: %s", threadName, message));
    }

    public abstract void process();

    public void submitFlag(String flag){
        print(String.format("submit flag '%s', response:\n%s", flag, FlagSubmitter.submit(flag)));
    }

    @Override
    public void run(){
        while (true) {
            process();
            try {
                Thread.sleep(frequency);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
