package com.singularity.jobs;

import com.singularity.tools.udp.client.UDPClient;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Lela on 05.09.2015.
 */
public class UDPTestJob extends Job {
    public UDPTestJob(String threadName, String address, long frequency) {
        super(threadName, address, frequency);
    }

    @Override
    public void process() {
        UDPClient client = new UDPClient();
        client.setupPort(8080);
        try {
            client.setupAddress(InetAddress.getByName("192.168.1.33"));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        String message = "Hello server, i'm client!";

        client.send(message);

        print("message sended");

        client.disconnect();
    }
}
