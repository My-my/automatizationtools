package com.singularity;

import com.singularity.jobs.*;
import com.singularity.tools.FlagSubmitter;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        List<Job> jobs = new ArrayList<Job>() {{
//            add(new AQprojectLoginJob("aq login", "http://torrent.aqproject.ru/", 30 * 1000));
            add(new SimpleClientExampleJob("SCJExample",
                    "https://www.google.ru/search?q=%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D0%B0&oq=htrkf&aqs=chrome.2.69i57j0l5.2954j0j4&sourceid=chrome&es_sm=93&ie=UTF-8",
                    30 * 1000));
        }};

        for (Job job : jobs) {
            new Thread(job).start();
        }
    }
}
